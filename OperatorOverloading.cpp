// OperatorOverloading.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>

using namespace std;

struct FVector
{
public:
	FVector()
	{
		this->x = 0.f;
		this->y = 0.f;
		this->z = 0.f;
	}

	FVector(const float x, const float y, const float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	operator float() const
	{
		return sqrt(x * x + y * y + z * z);
	}
	
	friend FVector operator+(const FVector& a, const FVector& b);

	friend FVector operator-(const FVector& a, const FVector& b);
	
	friend FVector operator*(const FVector& a, float b);
	
	friend ostream& operator<<(ostream& out, const FVector& a);

	friend istream& operator>>(istream& in, FVector& a);
	
	friend bool operator>(const FVector& a, const FVector& b);

	float operator[](int index) const
	{
		switch (index)
		{
		case 1:
			return x;
		break;
			
		case 2:
			return y;
		break;

		case 3:
			return z;
		break;

		default:
			cout << "Invalid index";
			return 0;
		}	
	}
	
private:
	float x;
	float y;
	float z;
};

FVector operator+(const FVector& a, const FVector& b)
{
	return FVector(a.x + b.x, a.y + b.y, a.z + b.z);
}

FVector operator-(const FVector& a, const FVector& b)
{
	return FVector(a.x - b.x, a.y - b.y, a.z - b.z);
}

FVector operator*(const FVector& a, float b)
{
	return FVector(a.x * b, a.y * b, a.z * b);
}

ostream& operator<<(ostream& out, const FVector& a)
{
	out << a.x << ' ' << a.y << ' ' << a.z;
	return out;
}

istream& operator>>(istream& in, FVector& a)
{
	in >> a.x >> a.y >> a.z;
	return in;
}

bool operator>(const FVector& a, const FVector& b)
{
	return false;
}

int main()
{
	FVector v1(0.f, 1.f, 2.f);
	FVector v2(3.f, 4.f, 5.f);
	FVector v3 = v1 + v2;
	FVector v4 = v1 * 2.f;;
	FVector v5 = v4 - v3;
	FVector v6;
	
	cout << "Sum of addition of v1 and v2 vectors: " << v3 << '\n'; 
	cout << "Sum of multiplication of v1 and v2 vectors: " << v4 << '\n';
	cout << "sum of subtraction of v4 and v3 vectors: " << v5 << '\n';
	cout << "v3 length: " << float(v3) << '\n';
	cout << "Enter a vector: ";
	cin >> v6;
	cout << "You entered: " << v6;
}
